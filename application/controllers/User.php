<?php

class User extends MY_Controller{

    public function profile(){

        $json = $this->getDomain();
        $json->url = $json->api;

        $this->load->view('layout/header2');
        // $this->load->view('utilisateur');
        $this->load->view('profile',$json);
        $this->load->view('layout/footer');

    }

    public function create(){

        $json = $this->getDomain();
        $json->url = $json->api;

        $this->load->view('layout/header2');
        $this->load->view('createUser',$json);
        $this->load->view('layout/footer');

    }

    public function createTrip(){

        $json = $this->getDomain();
        $json->url = $json->api;

        $this->load->view('layout/header2');
        $this->load->view('createTrip',$json);
        // $this->load->view('map');
        $this->load->view('layout/footer');

    }

    public function updateTrip(){

        $json = $this->getDomain();
        $json->url = $json->api;

        $this->load->view('layout/header2');
        $this->load->view('updateTrip',$json);
        $this->load->view('layout/footer');

    }

    public function delete(){

    

    }

}