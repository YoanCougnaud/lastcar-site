<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller{

    public function index(){

        $json = $this->getDomain();
        $json->url = $json->api;

        $this->load->view('layout/header2');
        $this->load->view('login', $json);
        $this->load->view('layout/footer');
    }

    public function accueil(){
        // if($user != "connected"){

            $this->load->view('layout/header2');
            $this->load->view('accueil');
            $this->load->view('layout/footer');
        // }else{
        //     $this->load->view('layout/header2');
        //     $this->load->view('accueil');
        //     $this->load->view('layout/footer');
        // }
    }

    public function trips(){

        $json = $this->getDomain();
        $json->url = $json->api;

        $this->load->view('layout/header2');
        $this->load->view('trips',$json);
        $this->load->view('layout/footer');
    }

    public function tripid(){

        $json = $this->getDomain();
        $json->url = $json->api;

        $this->load->view('layout/header2');
        $this->load->view('tripid',$json);
        $this->load->view('layout/footer');
    }

    public function payments(){

        $json = $this->getDomain();
        $json->url = $json->api;

        $this->load->view('layout/header2');
        $this->load->view('payment',$json);
        $this->load->view('layout/footer');
    }

    public function reviews(){

        $json = $this->getDomain();
        $json->url = $json->api;

        $this->load->view('layout/header2');
        $this->load->view('review',$json);
        $this->load->view('layout/footer');
    }


    public function logout(){
        
    }

}