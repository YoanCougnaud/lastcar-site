<?php

class Trip extends MY_Controller{

    public function getall(){

        $json = $this->getDomain();
        $json->url = $json->api;

        $this->load->view('layout/header2');
        $this->load->view('search',$json);
        // $this->load->view('search2');
        $this->load->view('layout/footer');

    }

    public function tripid(){

        $json = $this->getDomain();
        $json->url = $json->api;
        
        $this->load->view('layout/header2');
        $this->load->view('searchid',$json);
        $this->load->view('layout/footer');

    }

}