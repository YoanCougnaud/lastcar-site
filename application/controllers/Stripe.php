<?php
// require_once('vendor/autoload.php');

class Stripe extends MY_Controller{

    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {

        parent::__construct();
        $this->load->library("session");
        $this->load->helper('url');

    }
    
    /**
     * Get All Data from this method.
    *
    * @return Response
    */
    public function index(){

        $json = $this->getDomain();
        $json->url = $json->api;
        $json->key = $json->stripeKeyJs;

        $this->load->view('layout/header2');
        $this->load->view('stripe/stripe3', $json);
        $this->load->view('layout/footer');

    }
    
}