<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/recap.css">
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css"> -->
    <title>Document</title>
</head>
<body>
<script src="https://checkout.stripe.com/checkout.js"></script>
<div class="container">
<!--permet l'envoie liés à un utilisateur + id trip-->
<form method="post" id="stripeform">
  <input type="hidden" id="firstname" name="firstname"/>
  <input type="hidden" id="lastname" name="lastname"/>
  <input type="hidden" id="email" name="email"/>
  <input type="hidden" id="trip_id" name="trip_id"/>
  <input type="hidden" id="user_id" name="user_id"/>
</form>
<div class="blocrecap">
    <!--c'est la que le récapitulatif est affiché-->
    <div id="recap">
      
    <div>
<div>
</div>
<script>
//récupère les infos liés à l'utilisateur dans le local storage
let user = localStorage.getItem("user");
if(user == null){

  window.location.replace("<?php echo base_url()?>connexion");

}else{
//parse user en objet
user = JSON.parse(user);
// récupère l'id du trip via l'uri
let tripid = window.location.pathname.split('/')[2];

console.log(tripid);
//récupère les infos liés à un trajet séléctionné lors de la recherche stocké dans le local storage
let trips = localStorage.getItem("searchid");

//les infos sont parsé en objet
let trip = JSON.parse(trips);
//récupération du prix du trajet
let prix = `${trip.prix}`;

//récupération de l'id récap
let recap = document.getElementById('recap');

//regex permettant de mettre un point après deux 0 pour avoir le bon format du prix du trajet à l'affichage
let prixRecap = prix.replace(/(\d)(?=(\d\d)+(?!\d))/g, "$1.");

//récapitulatif des données liés au trajet stocké dans un tableau pour affichage sur la page 
let output = `<ul id="recapul">
                <h3 id="recap" style="color:#000; font-size: 30px; padding-top: 10%; padding-bottom: 10%; margin-left: 0%; font-weight: bolder;">Récapitulatif</h3>
                <li class="recapli" style="background-color: #fff" id="city_from">${trip.city_from}</li>
                <li class="recapli" id="city_to">${trip.city_to}</li>
                <li class="recapli" style="background-color: #fff" id="date">${trip.date}</li>
                <li class="recapli" id="price">${prixRecap}€</li>
                <button class="btn btn-primary" style="font-weight: bold" id="customButton">Payer ${prixRecap}€</button>
              </ul>`;

// permet d'afficher le récapitulatif dans l'id recap
recap.innerHTML = output;

console.log(user.email);

//tableau d'objet liés au formulaire en récupérérant les id des input + id button
const stripeform = {

  firstname : document.getElementById('firstname'),
  lastname : document.getElementById('lastname'),
  email : document.getElementById('email'),
  tripid : document.getElementById('trip_id'),
  userid : document.getElementById('user_id'),
  submit : document.getElementById('customButton')

}

//ajoute les infos (valeurs) liés à l'utilisateur + id trajet dans les id du formulaire
document.getElementById('firstname').value = `${user.firstname}`;
document.getElementById('lastname').value = `${user.lastname}`;
document.getElementById('email').value = `${user.email}`;
document.getElementById('trip_id').value = `${trip.id}`;
document.getElementById('user_id').value = `${user.id}`;

let domain = "<?php echo $url ?>";
let stripeKey = "<?php echo $key ?>";

console.log(stripeKey);

//configuration du checkout liés à stripe
var handler = StripeCheckout.configure({
  //clé test lié à un utilisateur sur stripe
  key: stripeKey,
  //image 
  image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
  locale: 'auto',
  //un token sera crée à la soumission
  token: function(token) {

    // envoie le token dans la requête ajax
    var xhr = new XMLHttpRequest();

    // envoie requête post vers l'api concernant les paiements
    xhr.open('POST', domain+'charge/'+tripid, true);

    //ajout du token liés à l'utilisateur dans le header
    xhr.setRequestHeader("authorization", user.jwt);

    //permet de dire qu'on envoie un formulaire
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    //partie permettant de récupérer les infos liés au paiement dès la soumission
    xhr.onload = function() {
      
      //si ok
      if (xhr.status === 200) {
        //récupère infos + parse
        var resp = JSON.parse(xhr.responseText);

        //affichage infos dans console
        console.log(resp);
        // alert(resp.form.token);
      }

      //si paiement echoue 
      else if (xhr.status !== 200) {
        alert('Request failed.  Returned status of ' + xhr.status);
      }
    };

    // les clé et valeurs des input firstname lastname email tripid userid et le token seront envoyés vers l'api 
    const requestData = `firstname=${stripeform.firstname.value}&lastname=${stripeform.lastname.value}&email=${stripeform.email.value}&trip_id=${stripeform.tripid.value}&user_id=${stripeform.userid.value}&stripeToken=${token.id}`;

    //envoie de la requête avec les infos à envoyer
    xhr.send(requestData);
  }
});

//permet d'afficher le checkout dès qu'on appuie sur le button submit de l'id customButton
stripeform.submit.addEventListener('click', function(e) {
  // Ouverture du checkout avec plusieurs option
  handler.open({
    name: 'Stripe.com',
    description: 'Transaction sécurisée',
    amount: prix
  });
  //empêche de soumettre
  e.preventDefault();
});

// permet de fermer le checkout dans le navigateur quand le paiement a été effectué
window.addEventListener('popstate', function() {
  handler.close();
});
}
</script>
</body>
</html>