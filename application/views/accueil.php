<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        body,html{
            margin: 0;
            padding: 0;
            background-color: #ddd;
        }
        #myChart{
            position: absolute;
            z-index: -1;
            top: 30px;
            right: 10px;
            transform : scale(0.8)
        }
    </style>
</head>
<body>
    <canvas id="myChart"></canvas>
</body>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script>
    var ctx = document.getElementById('myChart').getContext('2d');
var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'line',

    // The data for our dataset
    data: {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
        datasets: [{
            label : 'Nombre d\'activité',
            backgroundColor: 'rgb(121, 231, 234)',
            borderColor: 'rgb(0, 1, 77)',
            data: [0, 10, 5, 2, 20, 30, 45]
        }]
    },

    // Configuration options go here
    options: {}
});
</script>
</html>