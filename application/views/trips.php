<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
<div style="background-color: #555; width:100%; height: 200px">
<div class="container">
    <div style="margin-top:120px">
        <h1 style="text-align: center; font-weight:bold; color: #fff">Mes trajets</h1><br>
</div> 
    <table class="table table-striped">
        <div id="searchWrapper">
            <label for="search">Rechercher dans la liste</label>
            <input style="margin-bottom: 20px; margin-top:30px" type="text" name="searchBar" id="searchBar"/>
        </div>
            <thead>
                <tr>
                    <td class="td">Id</td>  
                    <td class="td">Départ</td>
                    <td class="td">Arrivée</td>
                    <td class="td">Date</td>
                    <td class="td">Prix</td>
                    <td class="td">Rendez-Vous</td>
                </tr>
            </thead>
            <tbody id="trips">
                <?php //ajax content ?>
            </tbody>
            <tbody id="trippassager">
                <?php //ajax content ?>
            </tbody>
    </table>
    </div>
</div>
<script>
list();


function list(){
//récupération des infos liés à l'utilisateur dans le localstorage
let user = localStorage.getItem("user");

if(user == null){

    window.location.replace("<?php echo base_url()?>connexion");

}else{

    let domain = "<?php echo $url ?>";
     //parse infos user
    user = JSON.parse(user);
    //instanciation de l'objet xhr
    let xhr = new XMLHttpRequest();
    //récupération des paiements liés à un utilisateur via l'uri qui point vers l'api 
    xhr.open('GET', domain+'user/'+user.id+'/trips', true);
    
    //ajout du token liés à l'utilisateur dans le header
    xhr.setRequestHeader("authorization", user.jwt);

    //section permettant la récupération des trajets liés à l'utilisateur et d'afficher sur la page 
    xhr.onload = function(){

        //récupération de l'id trips
        const tripList = document.getElementById('trips');

        //récupération de l'id trippassager
        const tripPassager = document.getElementById('trippassager');

        //récupération de l'id searchBar
        const searchBar = document.getElementById('searchBar');

        //array
        let trips = [];
        //addeventlistener on keyup permettant de récupérer des infos en écrivant dans l'input
        searchBar.addEventListener('keyup' ,(e) => {
            //récupération des valeurs tapés dans l'inputs 
            const searchString = e.target.value.toLowerCase();
            //filtrage des infos récupéré lors de la requête ajax
            const filteredTrips = trips.filter((trip) => {
                 //comparaison des valeurs tapés dans l'input et la valeur récupéré lors de la réquête ajax pour récupéré les infos liés à la valeur entrée dans l'input
                return trip.city_from.toLowerCase().includes(searchString) || trip.city_to.toLowerCase().includes(searchString) || trip.date.toLowerCase().includes(searchString) || trip.pickup.toLowerCase().includes(searchString) 

            });
            //envoie des infos en paramètre dan displayTrips
            displayTrips(filteredTrips);

        });

        //récupération des infos liés à l'utilisateur 
        const loadTrips = () => {

            //si 401 redirection
            if(xhr.status == 401){
                
                window.location.replace("<?php echo base_url()?>connexion");
               
            }else{
                //récupère réponse puis parse en objet
                trips = JSON.parse(xhr.responseText);

                //envoie les infos dans displayTrips
                displayTrips(trips);

            }
        }

        //permet d'afficher les données récupérés lors de la requête ajax dans l'api puis affichage des infos dans la page 
        const displayTrips = (trip) => {

            //récupération des données liés au conducteur 
            let tripconducteur = trip['conducteur'];
    
            let output = '';

            //boucle sur tripconducteur pour afficher tout les infos liés au conducteur
            for(let i in tripconducteur){

                 //conversion prix avec un point après les deux derniers 0
                let tripprix = tripconducteur[i].prix.replace(/(\d)(?=(\d\d)+(?!\d))/g, "$1.");

                output += 

                    '<tr>'+
                        '<td>'+tripconducteur[i].id+'</td>'+
                        '<td>'+tripconducteur[i].city_from+"</td>"+
                        '<td>'+tripconducteur[i].city_to+'</td>'+
                        '<td>'+tripconducteur[i].date+'</td>'+
                        '<td>'+tripprix+'</td>'+
                        '<td>'+tripconducteur[i].pickup+'</td>'+
                        '<td><a href="<?php echo base_url()?>montrajet/'+tripconducteur[i].id+'" class="btn btn-primary" style="float: right;">show</a></td>'+
                        '<td><a href="<?php echo base_url()?>tripupdate/'+tripconducteur[i].id+'" class="btn btn-primary";>update</a></td>'+
                    '</tr>';
            }
            ////envoie de output dans l'id trips pour l'affichage
            tripList.innerHTML = output;

            // //récupération des données liés au passager 
            // let trippassager = trip['passager'];

            // let output2 = '';

            // //boucle sur trippassager pour afficher tout les infos liés au passager
            // for(let i in trippassager){

            //     let tripprice = trippassager[i].price.replace(/(\d)(?=(\d\d)+(?!\d))/g, "$1.");

            //     //stockage de l'affichage des infos dans une table
            //     output2 += 

            //         '<tr>'+
            //             '<td>'+trippassager[i].id+'</td>'+
            //             '<td>'+trippassager[i].city_from+"</td>"+
            //             '<td>'+trippassager[i].city_to+'</td>'+
            //             '<td>'+trippassager[i].date+'</td>'+
            //             '<td>'+tripprice+'</td>'+
            //             '<td>'+trippassager[i].pickup+'</td>'+
            //             '<td><a href="<?php echo base_url()?>montrajet/'+trippassager[i].id+'" class="btn btn-primary" style="float: right;">show</a></td>'+
            //         '</tr>';

            // }
            // //envoie de output dans l'id trippassager pour l'affichage
            // tripPassager.innerHTML = output2;

        };

        loadTrips();

    }
    //envoie de la requête
    xhr.send();
}
}
</script>
</body>
</html>