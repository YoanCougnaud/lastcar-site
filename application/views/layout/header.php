<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css">
    <title>Document</title>
</head>
<body>
<nav>
    <div class="container">
        <input type="checkbox" id="check">
        <label for="check" class="checkbtn">
            <i class="glyphicon glyphicon-menu-hamburger"></i>
        </label>
        <label id="logo" for="logo">Lastcar</label>
        <ul>
            <li><a href="<?php echo base_url()?>accueil" class="lien">Rechercher</a></li>
            <li><a href="<?php echo base_url()?>users" class="lien">Proposer un trajet</a></li>
            <li><a href="<?php echo base_url()?>events" class="lien">Se connecter</a></li>
        </ul>
</nav>
    
