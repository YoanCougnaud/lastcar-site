<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css">
    <title>Document</title>
</head>
<body>
<div class="wrapper hover_collapse">
    <nav>
        <div class="container">
            <label id="logo" for="logo">Lastcar</label>
            <ul>
                <li><a href="<?php echo base_url()?>search" class="lien proposer">Rechercher</a></li>
                <li><a href="<?php echo base_url()?>tripcreate" class="lien proposer">Proposer un trajet</a></li>
                <li id="profiletd">
                    <section id="picturediv"></section>
                    <section id="name"></section>
                </li>
            </ul>
    </nav>
    <div id="side" class="sidebar">
        <ul>
            <li>
                <a href="<?php echo base_url()?>utilisateur" class="lienside">
                <div class="border">
                    <span class="icon">
                        <i class="glyphicon glyphicon-user"></i>
                    </span>
                    <span class="text">Mon profil</span>
                </div>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url()?>mestrajets" class="lienside">
                    <span class="icon">
                        <i class="glyphicon glyphicon-road"></i>
                    </span>
                    <span class="text">Mes trajets</span>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url()?>mespaiements" class="lienside">
                    <span class="icon">
                        <i class="glyphicon glyphicon-euro"></i>
                    </span>
                    <span class="text">Mes paiements</span>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url()?>mesavis" class="lienside">
                    <span class="icon">
                        <i class="glyphicon glyphicon-heart"></i>
                    </span>
                    <span class="text">Mes avis</span>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url()?>connexion" class="lienside" id="disconnect">
                    <span class="icon">
                        <i class="glyphicon glyphicon-send"></i>
                    </span>
                    <span class="text">Déconnecter</span>
                </a>
            </li>
        </ul>
    </div>
</div>
<script>

    disconnect();

    function disconnect(){

        let disconnect = document.getElementById('disconnect');
        // let profile = document.getElementsByClassName('sidebar').style.marginLeft= "100px";
        // var profile = document.getElementById('side').style; 
       
        disconnect.addEventListener('click', () =>{

            localStorage.clear();
            // profile.position = "fixed";
            // profile.right = "-100px";

            // profile.classList.remove('sidebar');
            // profile.remove();

        });
        
    }

    sidebar();

    function sidebar(){

        var li_items = document.querySelectorAll(".sidebar ul li");

        li_items.forEach((li_item)=>{
            li_item.addEventListener("mouseenter", ()=>{
                li_item.closest(".wrapper").classList.remove("hover_collapse")
            })
        })

        li_items.forEach((li_item)=>{
            li_item.addEventListener("mouseleave", ()=>{
                li_item.closest(".wrapper").classList.add("hover_collapse")
            })
        })

    }

    profile();

    function profile(){

        let image = document.getElementById("picturediv");

        let user = localStorage.getItem("user");

        user = JSON.parse(user);

        // image.innerHTML = `<img id="picture" src="../assets/images/empty.jpg" alt="photo">`;

        image.innerHTML = `<img id="picture" src="${user.picture}" alt="photo">`;

        document.getElementById("name").innerHTML = user.firstname+' '+user.lastname;

    }

    // let local = localStorage;

    // console.log(local.user);

    // checklocalstorage();

    // function checklocalstorage(){

    //    var a_items = document.querySelectorAll('.sidebar ul li a .lienside');
        
    //     a_items.forEach(a_item => {

    //         a_item.addEventListener('click', () =>{
                    
    //             console.log("laaaaaaaaaaaaaaaaaaaaaaaaaa");
    //             // if( "user" in localStorage){
                    
    //             //     // window.location.href("<?php echo base_url()?>connexion");
    //             //     console.log('yessss');
                    
    //             // }else{
    //             //     console.log('no')
    //             // }

    //         })

    //     })
             
    // }
    
</script>