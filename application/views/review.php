<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
<div style="background-color: #555; width:100%; height: 200px">
<div class="container">
    <div style="margin-top:120px">
        <h1 style="text-align: center; font-weight:bold; color: #fff">Mes avis</h1><br>
</div> 
    <table class="table table-striped">
        <div id="searchWrapper">
            <label for="search">Rechercher dans la liste</label>
            <input style="margin-bottom: 20px; margin-top:30px" type="text" name="searchBar" id="searchBar"/>
        </div>
            <thead>
                <tr>
                    <td class="td">Nom</td>
                    <td class="td">Prenom</td>
                    <td class="td">Note</td>
                    <td class="td">Commentaire</td>
                    <!-- <td class="td">User_id</td> -->
                </tr>
            </thead>
            <tbody id="review">
                <?php //ajax content ?>
            </tbody>
    </table>
    </div>
</div>
<script>
list();


function list(){

let user = localStorage.getItem("user");

if(user == null){

    window.location.replace("<?php echo base_url()?>connexion");

}else{
    
    let domain = "<?php echo $url ?>";
    
    user = JSON.parse(user);

    let xhr = new XMLHttpRequest();

    // xhr.open('GET', 'http://lastcar-api.bwb/user/'+user.id+'/reviews', true);
    xhr.open('GET', domain+'user/'+user.id+'/payments', true);
    
    xhr.setRequestHeader("authorization", user.jwt);

    xhr.onload = function(){

        const tripList = document.getElementById('review');

        const searchBar = document.getElementById('searchBar');

        let review = [];

        searchBar.addEventListener('keyup' ,(e) => {

            const searchString = e.target.value.toLowerCase();

            const filteredTrips = review.filter((review) => {

                return review.city_from.toLowerCase().includes(searchString) || review.city_to.toLowerCase().includes(searchString) || review.date.toLowerCase().includes(searchString) || review.pickup.toLowerCase().includes(searchString) 

            });

            displayreviews(filteredTrips);

        });

        const loadreviews = () => {
            
            if(xhr.status == 401){
                
                window.location.replace("<?php echo base_url()?>connexion");
               
            }else{
                
                // review = JSON.parse(xhr.responseText);

                // displayreviews(review);


                let review = JSON.parse(xhr.responseText);

                displayreviews(review);
                
                console.log(review);
            }
        }

        const displayreviews = (review) => {

            var output = '';
            
            for(var i in review){

                // output += 

                //     '<tr>'+
                //         // '<td>'+review[i].city_from+"</td>"+
                //         // '<td>'+review[i].city_to+'</td>'+
                //         // '<td>'+amountreview+'</td>'+
                //         // '<td>'+review[i].currency+'</td>'+
                //         // '<td>'+review[i].user_id+'</td>'+
                //         // '<td><a href="<?php echo base_url()?>montrajet/'+review[i].id+'" class="btn btn-primary" style="float: right;">show</a></td>'+
                //         // '<td><a href="<?php echo base_url()?>tripupdate/'+review[i].id+'" class="btn btn-primary";>update</a></td>'+
                //     '</tr>';

                output += 

                    '<tr>'+
                        '<td>'+review[i].city_from+"</td>"+
                        '<td>'+review[i].city_to+'</td>'+
                        '<td>'+review[i].amount+'</td>'+
                        '<td>'+review[i].currency+'</td>'+
                        // '<td>'+review[i].user_id+'</td>'+
                        // '<td><a href="<?php echo base_url()?>montrajet/'+review[i].id+'" class="btn btn-primary" style="float: right;">show</a></td>'+
                        // '<td><a href="<?php echo base_url()?>tripupdate/'+review[i].id+'" class="btn btn-primary";>update</a></td>'+
                    '</tr>';

            }

            tripList.innerHTML = output;

        };

        loadreviews();

    }

    xhr.send();
}
}
</script>
</body>
</html>