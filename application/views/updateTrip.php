<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.datetimepicker.min.css">
    <!-- <link rel="stylesheet" type="text/css" href="<?php base_url();?>assets/css/bootstrap.min.css"> -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/form.css">
    <title>Document</title>
</head>
<body>
    <div class="container">
        <a href="<?php echo base_url()?>events" class="glyphicon glyphicon-arrow-left" style="font-size: 50px; text-decoration: none"></a>
        <div class="blocform">
            <div class="form">
                <h3 id="title" style="text-align: center">Mettre à jour un trajet</h3><br>
                <div id="map"></div>
                <form method="post" id="formUpdate">
                    <input class="formulaire" type="hidden" name="id" id="id" placeholder="id"/>
                    <div class="control">
                        <input class="formulaire"  type="text" name="cityfrom" id="cityfrom" placeholder="Partir de"/>
                        <p class="perror">Error message</p>
                    </div>
                    <div class="control">
                        <input class="formulaire"  style="float:right" type="text" name="cityto" id="cityto" placeholder="Aller à"/>
                        <p class="perror">Error message</p>
                    </div>
                    <div class="control">
                        <input class="formulaire"  type="text" name="date" id="date" placeholder="Date"/>
                        <p class="perror">Error message</p>
                    </div>
                    <div class="control">
                        <input class="formulaire"  type="text" name="pickup" id="pickup" placeholder="Rendez-Vous"/>
                        <p class="perror">Error message</p>
                    </div>
                    <div class="control">
                        <input class="formulaire"  type="text" name="prix" id="prix" placeholder="Prix"/>
                        <p class="perror">Error message</p>
                    </div>
                    <input class="formulaire" type="hidden" name="userid" id="userid" placeholder="user_id"/>
                    <div id="infowindow-content">
                        <img src="" width="16" height="16" id="place-icon">
                        <span id="place-name" class="title"></span><br>
                        <span id="place-address"></span>
                    </div>
                </form>
                <button type="submit" class="btn btn-primary" id="btn-submit">Mettre à jour</button>
            </div>
        </div>
    </div>
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.4.3/umd/popper.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.datetimepicker.full.min.js"></script>
<script src="<?php echo base_url();?>assets/js/map.js"></script>
<script>
let user = localStorage.getItem("user");

if(user == null){

    window.location.replace("<?php echo base_url()?>connexion");

}else{

    user = JSON.parse(user);

    //library jquery permettant de choisir des dates et des heures
    $('#date').datetimepicker({

        timepicker: true,
        datepicker: true,
        format: 'Y-m-d H:i:00',
        value: 'defaultTime',
        hours12: false,
        step : 15,
        yearStart: 2020,
        yearEnd: 2025,
        minDate: '0',
        minTime:'0'
    });

    //récupération de la div l'id trips et stockage dans une variable
    const tripList = document.getElementById('trips');

    //récupération de l'id trip de l'uri dans lequel nous sommes
    let tripid = window.location.pathname.split('/')[2];

    console.log(tripid);

    //instanciation ajax xhr
    let xhr = new XMLHttpRequest();

    //récupération des données liés au trajet qui a été crée par l'utilisateur
    xhr.open('GET', 'http://lastcar-api.bwb/trip/'+tripid, true);

    xhr.setRequestHeader("authorization", user.jwt);

    //préparation de la réponse
    xhr.onload = function(){

        //si ok
        if(this.status == 200){

            //parse en objet la réponse récupéré
            let trip = JSON.parse(this.responseText);

            console.log(trip);

            //insertion des valeurs liés aux infos récupérés dans les id correspondant au input du formulaire  
            document.getElementById('cityfrom').value = `${trip.city_from}`;
            document.getElementById('cityto').value = `${trip.city_to}`;
            // document.getElementById('pickup').value = `${trip.date}`;
            document.getElementById('pickup').value = `${trip.pickup}`;
            document.getElementById('prix').value = `${trip.prix}`;
            document.getElementById('userid').value = `${trip.user_id}`;

        }

    }

    //envoie de la requête
    xhr.send();
    
}
</script>
<script>

    let domain = "<?php echo $url ?>";

    let users = localStorage.getItem("user");

    users = JSON.parse(users);

    //récupération de l'id trip de l'uri dans lequel nous sommes
    let tripId = window.location.pathname.split('/')[2];

    //insertion de la valeur de l'id trip dans l'input correspondant à l'id
    document.getElementById('id').value = tripId;

    // let users = localStorage.getItem("user");

    // users = JSON.parse(users);

    //tableau d'objet liés au form
    const formUpdate = {

        id : document.getElementById('id'),
        cityfrom : document.getElementById('cityfrom'),
        cityto : document.getElementById('cityto'),
        date : document.getElementById('date'),
        prix : document.getElementById('prix'),
        pickup : document.getElementById('pickup'),
        userid : document.getElementById('userid'),
        submit : document.getElementById('btn-submit')

    }

    //function montrant une erreur 
    function showError(input, message){
        //selectionne le parent du l'input qui est la div pour class name control
        const control = input.parentElement;
        //ajoute le nom de class control error si il y a une erreur
        control.className = "control error";
        //selectionne le classe name perror
        const perror = control.querySelector('.perror');
        // ajoute le message d'erreur à la class perror 
        perror.innerText = message;
    }

    //function montrant un succès
    function showSuccess(input){
        //selectionne le parent de input qui est la div pour class control
        const control = input.parentElement;
        //ajoute à la class control : control success
        control.className = "control success";
    }

    function checkRequired(inputArr){
        inputArr.forEach(function(input){
            if(input.value === ''){
                showError(input, `${getFieldName(input)} est requis`)
            }else{
                showSuccess(input);
            }
        });
    }

    function getFieldName(input){
        return input.id.charAt(0).toUpperCase() + input.id.slice(1);
    }

    //addeventlistener au clique lors soumission du formulaire pour envoie les valeurs entrée dans input
    formUpdate.submit.addEventListener('click', (e) =>{

        // e.preventDefault();

        checkRequired([formUpdate.cityfrom, formUpdate.cityto, formUpdate.date, formUpdate.prix, formUpdate.pickup]);

        //instanciation ajax xhr
        const xhr = new XMLHttpRequest();

        // chargement des réponses récupéré à la soumission du formulaire
        xhr.onload = () => {

            if(xhr.status == 401){
                
                window.location.replace("<?php echo base_url()?>connexion");
               
            }else{

                if(xhr.status == 200){
                    window.location.replace("<?php echo base_url()?>mestrajets");
                }
                
            }

        }

        //permet de récupérer les valeurs liés au formulaire pour envoie sur api
        const requestData = `id=${formUpdate.id.value}&city_from=${formUpdate.cityfrom.value}&city_to=${formUpdate.cityto.value}&date=${formUpdate.date.value}&prix=${formUpdate.prix.value}&pickup=${formUpdate.pickup.value}&user_id=${formUpdate.userid.value}`;

        console.log(requestData);
         //envoie sur l'uri ci-dessous
        xhr.open('PUT', domain+'trip', true);

        xhr.setRequestHeader("authorization", users.jwt);

        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        //envoie des valeurs liés au formulaire lors de la requête 
        xhr.send(requestData);

    });

</script>
</body>
</html>