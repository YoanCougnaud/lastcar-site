<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- <link rel="stylesheet" type="text/css" href="<?php base_url();?>assets/css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.datetimepicker.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/form.css">
    <title>Document</title>
</head>
<body>
    <div class="container">
        <!-- <a href="<?php echo base_url()?>events" class="glyphicon glyphicon-arrow-left" style="font-size: 50px; text-decoration: none"></a> -->
        <div class="blocform">
            <div class="form">
                <h3 id="title" style="text-align: center">Créer un trajet</h3><br>
                <div id="map"></div>
                <form method="post" id="formCreate">
                <div class="control">
                    <input class="formulaire"  type="text" name="cityfrom" id="cityfrom" placeholder="Partir de"/>
                    <p class="perror">Error message</p>
                </div>
                <div class="control">
                    <input class="formulaire"  style="float:right" type="text" name="cityto" id="cityto" placeholder="Aller à"/>
                    <p class="perror">Error message</p>
                </div>
                <div class="control">
                    <input class="formulaire"  type="text" name="date" id="date" placeholder="Date"/>
                    <p class="perror">Error message</p>
                </div>
                <div class="control">
                    <input class="formulaire"  type="text" name="pickup" id="pickup" placeholder="Rendez-Vous"/>
                    <p class="perror">Error message</p>
                </div>
                <div class="control">
                    <input class="formulaire"  type="text" name="prix" id="prix" placeholder="Prix"/>
                    <p class="perror">Error message</p>
                </div>
                    <input class="formulaire"  type="hidden" name="userid" id="userid"/>
                    <div id="infowindow-content">
                        <img src="" width="16" height="16" id="place-icon">
                        <span id="place-name" class="title"></span><br>
                        <span id="place-address"></span>
                    </div>
                </form>
                <button type="submit" class="btn btn-primary" id="btn-submit">Créer</button>
            </div>
        </div>
    </div>
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.4.3/umd/popper.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.datetimepicker.full.min.js"></script>
<script src="<?php echo base_url();?>assets/js/map.js"></script>
<script>

let domain = "<?php echo $url ?>";

//récupération des infos stocké liés à l'utilisateur dans le local storage
let users = localStorage.getItem("user");

if(users == null){
    window.location.replace("<?php echo base_url()?>connexion");
}else{

    //parse user en objet
    users = JSON.parse(users);

    //library jquery permettant de choisir des dates et des heures
    $('#date').datetimepicker({

        timepicker: true,
        datepicker: true,
        format: 'Y-m-d H:i:00',
        value: 'defaultTime',
        hours12: false,
        step : 15,
        yearStart: 2020,
        yearEnd: 2025,
        minDate: '0'
        // minTime:'1'
    });

    //tableau d'objet liés au form
    const formCreate = {

        cityfrom : document.getElementById('cityfrom'),
        cityto : document.getElementById('cityto'),
        date : document.getElementById('date'),
        prix : document.getElementById('prix'),
        pickup : document.getElementById('pickup'),
        userid : document.getElementById('userid'),
        submit : document.getElementById('btn-submit')

    }

    //insertion de l'id de l'utilisateur créant le trajet (celui étant connecté)
    document.getElementById('userid').value = `${users.id}`;

    //function montrant une erreur 
    function showError(input, message){
        //selectionne le parent du l'input qui est la div pour class name control
        const control = input.parentElement;
        //ajoute le nom de class control error si il y a une erreur
        control.className = "control error";
        //selectionne le classe name perror
        const perror = control.querySelector('.perror');
        // ajoute le message d'erreur à la class perror 
        perror.innerText = message;
    }

    //function montrant un succès
    function showSuccess(input){
        //selectionne le parent de input qui est la div pour class control
        const control = input.parentElement;
        //ajoute à la class control : control success
        control.className = "control success";
    }

    function checkRequired(inputArr){
        inputArr.forEach(function(input){
            if(input.value === ''){
                showError(input, `${getFieldName(input)} est requis`)
            }else{
                showSuccess(input);
            }
        });
    }

    function getFieldName(input){
        return input.id.charAt(0).toUpperCase() + input.id.slice(1);
    }

    //addeventlistener au clique lors soumission du formulaire pour envoie les valeurs entrée dans input
    formCreate.submit.addEventListener('click', (e) =>{

        e.preventDefault();

        checkRequired([formCreate.cityfrom, formCreate.cityto, formCreate.date, formCreate.prix, formCreate.pickup]);

        //instanciation ajax xhr
        const xhr = new XMLHttpRequest();

        // chargement des réponses récupéré à la soumission du formulaire
        xhr.onload = () => {

            if(xhr.status == 401){
                
                window.location.replace("<?php echo base_url()?>connexion");
               
            }else{

                //variable vide
                let responseObject = null;

                //si succès récupération réponse
                try {

                    responseObject = JSON.parse(xhr.responseText);

                //sinon méssage d'erreur
                } catch (e) {

                    console.error('could not parse JSON!')

                }

                //si il y a réponse envoie sur la fonction handleResponse via paramètre
                if(responseObject) {

                    handleResponse(responseObject);

                }
                
            }

        }

        //permet de récupérer les valeurs liés au formulaire pour envoie sur api
        const requestData = `city_from=${formCreate.cityfrom.value}&city_to=${formCreate.cityto.value}&date=${formCreate.date.value}&prix=${formCreate.prix.value}&pickup=${formCreate.pickup.value}&user_id=${formCreate.userid.value}`;

        console.log(requestData);

        //envoie sur l'uri ci-dessous
        xhr.open('POST', domain+'trip', true);

        xhr.setRequestHeader("authorization", users.jwt);

        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        //envoie des valeurs liés au formulaire lors de la requête 
        xhr.send(requestData);

    });

    //récupération réponse puis affichage dans la console
    function handleResponse(responseObject){

        let x = responseObject;

        if(x == true){
            window.location.replace("<?php echo base_url()?>accueil");
        }
    }
}
</script>
</body>
</html>