<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
<div style="background-color: #555; width:100%; height: 200px">
<div class="container">
    <div style="margin-top:120px">
        <h1 style="text-align: center; font-weight:bold; color: #fff">Mes paiements</h1><br>
</div> 
    <table class="table table-striped">
        <div id="searchWrapper">
            <label for="search">Rechercher dans la liste</label>
            <input style="margin-bottom: 20px; margin-top:30px" type="text" name="searchBar" id="searchBar"/>
        </div>
            <thead>
            <!--affichage titres des infos récupérés dans une table-->
                <tr>
                    <td class="td">Ville départ</td>
                    <td class="td">Ville arrivé</td>
                    <td class="td">Somme</td>
                    <td class="td">Monnaie</td>
                    <!-- <td class="td">User_id</td> -->
                </tr>
            </thead>
            <tbody id="payment">
                <?php //ajax content ?>
            </tbody>
    </table>
    </div>
</div>
<script>
list();


function list(){
    
    //récupération des infos liés à l'utilisateur connecté dans le local storage
let user = localStorage.getItem("user");

if(user == null){

    window.location.replace("<?php echo base_url()?>connexion");

}else{

    let domain = "<?php echo $url ?>";
    
    //parse infos user
    user = JSON.parse(user);

    //instanciation de l'objet xhr
    let xhr = new XMLHttpRequest();

    //récupération des paiements liés à un utilisateur via l'uri qui point vers l'api 
    xhr.open('GET', domain+'user/'+user.id+'/payments', true);

    //ajout du token liés à l'utilisateur dans le header
    xhr.setRequestHeader("authorization", user.jwt);

    //section permettant la récupération des paiements liés à l'utilisateur et d'afficher sur la page 
    xhr.onload = function(){

        //récupération de l'id payment
        const tripList = document.getElementById('payment');

        //récupération de l'id searchBar
        const searchBar = document.getElementById('searchBar');

        //array
        let payment = [];

        //addeventlistener on keyup permettant de récupérer des infos en écrivant dans l'input
        searchBar.addEventListener('keyup' ,(e) => {

            //récupération des valeurs tapés dans l'inputs 
            const searchString = e.target.value.toLowerCase();

            //filtrage des infos récupéré lors de la requête ajax
            const filteredPayments = payment.filter((payment) => {

                //comparaison des valeurs tapés dans l'input et la valeur récupéré lors de la réquête ajax pour récupéré les infos liés à la valeur entrée dans l'input
                return payment.city_from.toLowerCase().includes(searchString) || payment.city_to.toLowerCase().includes(searchString) || payment.date.toLowerCase().includes(searchString) || payment.pickup.toLowerCase().includes(searchString) 

            });

            //envoie des infos en paramètre dan displayPayments
            displayPayments(filteredPayments);

        });

        //récupération des infos liés à l'utilisateur 
        const loadPayments = () => {

            //si 401 redirection
            if(xhr.status == 401){
                
                window.location.replace("<?php echo base_url()?>connexion");
               
            }else{
                
                //récupère réponse puis parse en objet
                payment = JSON.parse(xhr.responseText);

                //envoie les infos dans displayPayments
                displayPayments(payment);
                
            }
        }

        //permet d'afficher les données récupérés lors de la requête ajax dans l'api puis affichage des infos dans la page paiement
        const displayPayments = (payment) => {

            var output = '';
            
            //boucle sur payment pour afficher tout les infos liés au payment
            for(var i in payment){

                //stockage du paiement dans variable pour conversion
                let amount = payment[i].amount;
                // variable du paiement est mis dans un regex pour pouvoir afficher le prix avec un point après deux zéros 
                let amountPayment = amount.replace(/(\d)(?=(\d\d)+(?!\d))/g, "$1.");

                //stockage de l'affichage des infos dans une table
                output += 

                    '<tr>'+
                        '<td>'+payment[i].city_from+"</td>"+
                        '<td>'+payment[i].city_to+'</td>'+
                        '<td>'+amountPayment+'</td>'+
                        '<td>'+payment[i].currency+'</td>'+
                        // '<td>'+payment[i].user_id+'</td>'+
                        // '<td><a href="<?php echo base_url()?>montrajet/'+payment[i].id+'" class="btn btn-primary" style="float: right;">show</a></td>'+
                        // '<td><a href="<?php echo base_url()?>tripupdate/'+payment[i].id+'" class="btn btn-primary";>update</a></td>'+
                    '</tr>';

            }

            //envoie de output dans l'id payment pour l'affichage
            tripList.innerHTML = output;

        };

        loadPayments();

    }

    //envoie de la requête
    xhr.send();
}
}
</script>
</body>
</html>