<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/searchid.css">
<title>Document</title>
<script type='text/javascript'>

//permet de reload une fois que la page est completement chargé, celà permet à la map de se mettre à jour. 
window.onload = function(){
    //si il y a pas de hash value
    if(!window.location.hash){
        //on stock l'uri mixé avec #loaded
        window.location = window.location + '#loaded';
        //une fois que l'uri avec #loaded apparait dans l'url on recharge la page
        window.location.reload();
    }
}

let map;
let directionsManager;

//recupération des infos liés à un trajet lors de la recherche
let trips = localStorage.getItem("searchid");

//parse trips en objet
let trip = JSON.parse(trips);

//affichage de la map avec traçage direction point A à B
function GetMap()
{
    //instanciation de l'objet microsoft map
    map = new Microsoft.Maps.Map('#myMap', {});

    //Load the directions module.
    Microsoft.Maps.loadModule('Microsoft.Maps.Directions', function () {
        //Instanciation de la map direction.
        directionsManager = new Microsoft.Maps.Directions.DirectionsManager(map);

        //Création du point de départ avec insertion de trip.city_from.
        let firstWaypoint = new Microsoft.Maps.Directions.Waypoint({ address: `${trip.city_from}` });
        directionsManager.addWaypoint(firstWaypoint);

        //Création du point de d'arrivée avec insertion de trip.city_to.
        let secondWaypoint = new Microsoft.Maps.Directions.Waypoint({ address: `${trip.city_to}`});
        directionsManager.addWaypoint(secondWaypoint);

        //spécification en quoi l'element de l'itinéraire sera affiché 
        directionsManager.setRenderOptions({ itineraryContainer: '#directionsItinerary' });

        //calcule de la directions.
        directionsManager.calculateDirections();
    });
}
</script>
</head>
<!-- <body onload="document.roadform.submit()"> -->
<body>
<div class="container">
    <div style="margin-top:50px">
        <div id="contain">
            <h1 style="text-align: center; font-weight:bold">Itinéraire du trajet</h1><br>
            <div id="map">
                <div id="myMap"></div>
            </div>
            <div id="searchid">
                <div id="trip">
                    <?php //ajax content ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

searchid();


function searchid(){
    
    let domain = "<?php echo $url ?>";
    
    //récupération et stockage de l'id trips 
    const tripList = document.getElementById('trip');
    
    //récupération de l'id de l'uri dans lequel on se trouve
    let tripid = window.location.pathname.split('/')[2];
    
    // userId = window.location.pathname;
    
    console.log(tripid);
    
    //instanciation xhr
    let xhr = new XMLHttpRequest();
    
    //uri sur lequel les infos sont récupéré avec ajout de l'id concernant le trip souhaité
    xhr.open('GET', domain+'searchtrip/'+tripid, true);

    // xhr.setRequestHeader("authorization", users.jwt);
    
    //chargement de la réponse
    xhr.onload = function(){
        
        //si ok
        if(this.status == 200){
            
            //récupération de la réponse
            let response = xhr.responseText;
            
            //stockage de la réponse dans localstorage 
            localStorage.setItem("searchid", response);
            
            //parse objet de la reponse
            let trip = JSON.parse(response);
            
            //stockage prix du trajet récupéré lors de la requête 
            let prix = trip.prix;
            
            //conversion prix avec un point après les deux derniers 0
            let tripprix = prix.replace(/(\d)(?=(\d\d)+(?!\d))/g, "$1.");
            
            //préparation de l'affichage de la réponse sur la page en tableau
            let output =   `<table>
                                <h3 style="text-align:center; font-weight:bold ; position: relative; top: 70px"> les Détails du trajet</h3>
                                <tr class="searchlu">
                                    <th class="searchlileft">Prénom</th>
                                    <th class="searchliright">Photo</th>
                                </tr>
                                <tr class="searchlu">
                                    <td class="searchlileft">${trip.firstname}</td>
                                    <td class="searchliright">${trip.picture}</td>
                                </tr>
                                <tr class="searchlu">
                                    <th class="searchlileft">Départ</th>
                                    <th class="searchliright">Arrivé</th>
                                </tr>
                                <tr class="searchlu">
                                    <td class="searchlileft">${trip.city_from}</td>
                                    <td class="searchliright">${trip.city_to}</td>
                                </tr">
                                <tr class="searchlu">
                                    <th class="searchlileft">Date</th>
                                    <th class="searchliright">Prix</th>
                                </tr>
                                <tr class="searchlu">
                                    <td class="searchlileft">${trip.date}</td>
                                    <td class="searchliright">${tripprix} €</td>
                                </tr>
                                <tr class="searchlu">
                                    <th class="searchlileft">Point de rendez-vous</th>
                                </tr>
                                <tr class="searchlu">
                                    <td class="searchlileft">${trip.pickup}</td>
                                </tr>
                                <ul style="position: relative; top: 280px; left: 100px">
                                    <li class="searchli"><a href="<?php echo base_url()?>paiement/${trip.id}" class="btn btn-primary" style="margin-top:10%; width:50%">Payer</a></li>
                                </ul>
                            </table>`;
            
            //envoie de l'output dans la div qui a pour id trips puis affichage 
            tripList.innerHTML = output;

        }
        
    }
    
    //envoie de la requête
    xhr.send();
  
}
</script>
</body>
</html>