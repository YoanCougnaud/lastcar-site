<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.datetimepicker.min.css">
    <link rel="stylesheet" type="text/css" href="<?php base_url();?>assets/css/list.css">
    <!-- <link rel="stylesheet" type="text/css" href="<?php base_url();?>assets/css/bootstrap.min.css"> -->
    <title>Document</title>
</head>
<body>
    <div class="container">
        <div id="search">
        <!-- <a href="<?php echo base_url()?>accueil" class="glyphicon glyphicon-arrow-left" style="font-size: 50px; text-decoration: none"></a> -->
        <h1 style="text-align: center; font-weight:bold">Où veux-tu aller ?</h1><br>
            <div id="searchWrapper">
                <form method="post" id="searchBar">
                        <input class="ident" type="text" name="cityfrom" id="cityfrom" placeholder="Départ"/>
                        <input class="ident" type="text" name="cityto" id="cityto" placeholder="Destination"/>
                        <input class="ident" type="text" name="date" id="date" placeholder="Date"/>
                </form>
                <button type="submit" id="btn-submit">Rechercher</button>
            </div>
            <div id="tripresult">
            </div>
            <div id="trips">
                <!-- <?php //ajax content ?> -->
            </div>
        </div>
    </div>
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.4.3/umd/popper.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.datetimepicker.full.min.js"></script>
<script src="<?php echo base_url();?>assets/js/map.js"></script>
<script>

search();


function search(){ 

    let domain = "<?php echo $url ?>";

    //library jquery permettant de choisir des dates et des heures
    $('#date').datetimepicker({

        timepicker: true,
        datepicker: true,
        format: 'Y-m-d H:i:00',
        // value: 'defaultTime',
        hours12: false,
        step : 15,
        yearStart: 2020,
        yearEnd: 2025,
        minDate: '0'

    });

    //tableau d'objet liés au form
    const searchBar = {

        cityfrom : document.getElementById('cityfrom'),
        cityto : document.getElementById('cityto'),
        date : document.getElementById('date'),
        submit : document.getElementById('btn-submit')

    }

    //addeventlistener au clique lors soumission du formulaire pour envoie des valeurs entrée dans input
    searchBar.submit.addEventListener('click', () =>{

        //instanciation ajax xhr
        const xhr = new XMLHttpRequest();

        // chargement des réponses récupéré à la soumission du formulaire
        xhr.onload = () => {

             //variable vide
            let responseObject = null;

            //si succès récupération réponse
            try {

                responseObject = JSON.parse(xhr.responseText);

            //sinon méssage d'erreur
            } catch (e) {

                console.error('could not parse JSON or retrieve data!')

            }

            //si il y a réponse envoie sur la fonction handleResponse via paramètre
            if(responseObject) {
                
                handleResponse(responseObject);

            }

        }

        //permet de récupérer les valeurs liés au formulaire pour envoie sur api
        const requestData = `city_from=${searchBar.cityfrom.value}&city_to=${searchBar.cityto.value}&date=${searchBar.date.value}`;

        console.log(requestData);

        //envoie sur l'uri ci-dessous
        xhr.open('POST', domain+'searchtrips', true);

        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        //envoie des valeurs liés au formulaire lors de la requête 
        xhr.send(requestData);

    });

    //récupération réponse puis affichage dans la page de recherche
    function handleResponse(responseObject){

        let trip = responseObject;

        console.log(trip);

        let output = '';

        //boucle sur les infos récupérés 
        for(var i in trip){

            //récupération du prix et stockage de l'info dans une variable pour conversion
            let prix = `${trip[i].prix}`;

            //regex permettant d'ajouter un point après les deux dernier 0 
            let tripprix = prix.replace(/(\d)(?=(\d\d)+(?!\d))/g, "$1.");

                    // <li><a href="<?php echo base_url()?>search/${trip[i].id}" class="btn btn-primary" style="float: right;">show</a></li>
                    // <li class="tripli bas">${trip[i].picture}</li>
            
            //affiche la liste des trajets matchant avec la recherche
            output += 

            `<div id="result"> 
                <h3 style="margin-left: 50px; font-weight:bold">${trip[i].date}</h3>
                <a href="<?php echo base_url()?>search/${trip[i].id}" style="text-decoration:none">
                    <ul id="tripul">
                        <li class="tripli haut">${trip[i].city_from}</li>
                        <li class="tripli haut" style="margin-left: 70px; color:#fff; background-color:#555"></li>
                        <li class="tripli haut" style="float:right">${trip[i].city_to}</li></br>
                        <li class="tripli bas"><img class="picturetrip" src="../assets/images/empty.jpg" alt="photo"></li>
                        <li class="tripli bas" style="margin-left: 5px ">${trip[i].firstname}</li>
                        <li class="tripli bas" style="float:right; font-size:30px">${trip[i].prix}€</li>
                    </ul>
                </a>
            </div>`;

        }

        //ajout de output dans la div ayant pour id trips
        document.getElementById('trips').innerHTML = output;
        // tripList.innerHTML = output;

        console.log(trip.length);
        console.log();

            //affichage du nombre de résultat concernant la recherche + villes
            let result =

                    `<div id="resultatrip">
                        <ul class="resultat">
                            <li class="resultatli">${trip[0].city_from}</li>
                            <li class="resultatli">-</li>
                            <li class="resultatli">${trip[0].city_to}</li>
                        </ul>
                        <ul class="resu">
                            <li class="res">Total  :  </li>
                            <li class="res">${trip.length}</li>
                        </ul>
                    </div>`;

        //ajout de result dans la div ayant pour id tripresult
        document.getElementById('tripresult').innerHTML = result;

    }
  
}

</script>
</body>
</html>