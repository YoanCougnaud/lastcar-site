<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?php base_url();?>assets/css/login.css">
    <!-- <link rel="stylesheet" type="text/css" href="<?php base_url();?>assets/css/bootstrap.min.css"> -->
    <title>Document</title>
</head>
<body>
    <div class="container">
        <div class="blocform">
            <div class="form">
                <form method="post" id="formLogin">
                    <p>Identifiez-Vous</p>
                    <div class="control">
                        <input class="ident" type="text" name="email" id="email" placeholder="E-mail"></br>
                        <p class="perror">Error message</p>
                    </div>
                    <div class="control">
                        <input class="ident" type="password" name="password" id="password"  placeholder="Mot de passe"></br>
                        <p class="perror">Error message</p>
                    </div>
                    <!-- <input type="submit" class="btn btn-primary col-12" id="btn-submit" value="Se connecter"> -->
                </form>
                <button type="submit" class="btn btn-primary col-12" id="btn-submit">Se connecter</button>
                <div style="text-align: center; margin-top: 10px">
                    <a href="<?php echo base_url()?>inscription" style="font-size: 20px; text-decoration: none">Inscription</a>
                </div>
            </div>
        </div>
    </div>
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<!-- <script src="<?php echo base_url();?>assets/js/ajaxTokenHeader.js"></script> -->
<script>

    let domain = "<?php echo $url ?>";

    console.log(domain);

    // console.log(document.domain);
    // création d'un tableau d'objet
    const formLogin = {

        //tableau d'objet contenant les id des input email, password et du bouton permettant la soumission du formulaire
        email : document.getElementById('email'),
        password : document.getElementById('password'),
        submit : document.getElementById('btn-submit')

    };

    //function montrant une erreur 
    function showError(input, message){
        //selectionne le parent du l'input qui est la div pour class name control
        const control = input.parentElement;
        //ajoute le nom de class control error si il y a une erreur
        control.className = "control error";
        //selectionne le classe name perror
        const perror = control.querySelector('.perror');
        // ajoute le message d'erreur à la class perror 
        perror.innerText = message;
    }

    //function montrant un succès
    function showSuccess(input){
        //selectionne le parent de input qui est la div pour class control
        const control = input.parentElement;
        //ajoute à la class control : control success
        control.className = "control success";
    }

    //function checkant si l'email rentré dans l'input est valide
    function isValidEmail(email){
        const checkEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return checkEmail.test(String(email).toLowerCase());
    }

    // addEventListener au clique sur le bouton du formalaire
    formLogin.submit.addEventListener('click', (e) =>{

        e.preventDefault();

        //si la valeur de input email est vide
        if(formLogin.email.value === ''){
            //montre erreur
            showError(formLogin.email, 'Un email est requis');
            //sinon si l'email est invalide 
        }else if(!isValidEmail(formLogin.email.value)){
            //montre erreur
            showError(formLogin.email, 'L\email n\'est pas valide' );
            //sinon montre succès
        }else{
            showSuccess(formLogin.email);
        }

        //si la valeur de input password est vide
        if(formLogin.password.value === ''){
            //montre erreur
            showError(formLogin.password, 'Un mot de passe est requis');
            //sinon montre message succès
        }else{
            showSuccess(formLogin.password);
        }

        //instanciation de l'objet xhr permettant les requêtes ajax
        var xhr = new XMLHttpRequest();

        //la partie onload est la partie permettant de récupérer des données au moment de la requête
        xhr.onload = () => {

            //varriable null
            let responseObject = null;

            //récupération des infos liés à l'utilisateur se connectant
            let response = xhr.responseText;

            //stockage des données récupéré dans le locage storage qui aura pour nom user
            localStorage.setItem("user", response);

            //récupération des données user dans le local storage 
            var original = localStorage.getItem("user");

            //il y a pas d'erreur on parse les données du local storage en objet
            try {

                responseObject = JSON.parse(original);

            //erreur on ne parse pas
            } catch (e) {

                console.error('could not parse JSON!')

            }

            //si les données on été récupéré et qu'il y a pas eu de problème lors du parsing
            if(responseObject) {
                
                // envoie des résultat en paramètre dans la function handleResponse pour récupération
                handleResponse(responseObject);

            }

        }

        //récupération des valeurs entrés dans les inputs email et password
        const requestData = `email=${formLogin.email.value}&password=${formLogin.password.value}`;

        console.log(requestData);

        //envoie de la requête sur l'uri ci-dessous en post 
        xhr.open('POST', domain+'connexion', true);

        //permet de dire que l'on envoie des données via un formulaire
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        //envoie des valeurs entrés dans les inputs 
        xhr.send(requestData);

    });

    //function permettant l'affichage des données récupéré à la fin de la requête
    function handleResponse(responseObject){

        let x = responseObject;

        //si les données sont égale à false ou mauvais identifiant
        if(x == "false" || x == "mauvais identifiant"){

            //on reste sur la page de connexion
            window.location.replace("<?php echo base_url()?>connexion");
        
        //sinon on nous redirige vers l'accueil
        }else{

            window.location.replace("<?php echo base_url()?>accueil");

        }

    }

</script>
</body>
</html>