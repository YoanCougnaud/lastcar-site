<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
    <div class="container">
        <h1>Accueil</h1>
    <section id="btn"></section>
    <h1 style="text-align: center; font-weight:bold">fiche de trajets</h1><br>
    <table class="table table-striped">
            <thead>
                <tr>
                    <td class="td">Id</td>  
                    <td class="td">De</td>
                    <td class="td">à</td>
                    <td class="td">Date</td>
                    <td class="td">Pickup</td>
                    <td class="td">User_id</td>
                </tr>
            </thead>
            <tbody id="trips">
                <?php //ajax content ?>
            </tbody>
    </table>
</div>
</body>
<script>

tripid();


function tripid(){

let user = localStorage.getItem("user");

if(user == null){

    window.location.replace("<?php echo base_url()?>connexion");

}else{
    
let domain = "<?php echo $url ?>";
    
    user = JSON.parse(user);

    const tripList = document.getElementById('trips');

    let tripid = window.location.pathname.split('/')[2];

    // userId = window.location.pathname;

    console.log(tripid);

    let xhr = new XMLHttpRequest();

    xhr.open('GET', domain+'user/'+user.id+'/trip/'+tripid, true);

    xhr.setRequestHeader("authorization", user.jwt);

    xhr.onload = function(){

        if(xhr.status == 401){
                
                window.location.replace("<?php echo base_url()?>connexion");
               
        }else{

            let trip = JSON.parse(this.responseText);

            console.log(trip);

            let output ='<tr>'+
                            '<td>'+trip.id+'</td>'+
                            '<td>'+trip.city_from+"</td>"+
                            '<td>'+trip.city_to+'</td>'+
                            '<td>'+trip.date+'</td>'+
                            '<td>'+trip.pickup+'</td>'+
                            '<td>'+trip.user_id+'</td>'+
                            // '<td><a href="<?php echo base_url()?>trip/'+trip.id+'/users" class="btn btn-primary" style="float:right">show trip</a></td>'+
                        '</tr>';

            tripList.innerHTML = output;

        }

    }

    xhr.send();
        
}
}

</script>
</html>