<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.datetimepicker.min.css">
    <!-- <link rel="stylesheet" type="text/css" href="<?php base_url();?>assets/css/bootstrap.min.css"> -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/form.css">
    <title>Document</title>
</head>
<body>
    <div style="background-color: #555; width:100%; height: 200px">
<div class="container">
    <div style="margin-top:120px">
    <h1 style="text-align: center; font-weight:bold; color: #fff">Mon profil</h1><br>
    </div> 
    <div class="blocuser">
        <div class="form">
            <!--permet de mofifier des données liés à un utilisateur connecté via sa page de profil-->
            <form method="post" id="formUser" enctype="multipart/form-data">
                    <input type="hidden" class="l" name="id" id="id" placeholder="Id">
                <div class="control">
                    <input type="text" class="l" name="firstname" id="firstname" placeholder="Prenom">
                    <p class="perror">Error message</p>
                </div>
                <div class="control">
                    <input type="text" class="l" name="lastname" id="lastname" placeholder="Nom">
                    <p class="perror">Error message</p>
                </div>
                <div class="control">
                    <input type="text" class="l" name="birthday" id="birthday" placeholder="Date de naissance">
                    <p class="perror">Error message</p>
                </div>
                <div class="control">
                    <input type="text" class="l" name="city" id="city" placeholder="Ville">
                    <p class="perror">Error message</p>
                </div>
                    <input type="text" class="l" name="pictures" id="pictures" placeholder="Photo">
                <div class="control">
                    <input type="text" class="l" name="email" id="email" placeholder="E-mail">
                    <p class="perror">Error message</p>
                </div>
                <div class="control">
                    <input type="text" class="l" name="phone" id="phone" placeholder="Telephone">
                    <p class="perror">Error message</p>
                </div>
                <div class="control">
                    <input type="password" class="l" name="password" id="password" placeholder="Mot de passe">
                    <p class="perror">Error message</p>
                </div>
                <input type="hidden" name="role_id" id="role_id">
            </form>
            <button type="submit" class="btn btn-primary" id="btn-submit">Mettre à jour profile</button>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.4.3/umd/popper.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.datetimepicker.full.min.js"></script>
<script>


//récupération des infos liés à un utilisateur via le localstorage 
let user = localStorage.getItem("user");

if(user == null){

    window.location.replace("<?php echo base_url()?>connexion");

}else{

    let domain = "<?php echo $url ?>";
    
    //library jquery permettant de choisir des dates et des heures
    $('#birthday').datetimepicker({

        timepicker: false,
        datepicker: true,
        format: 'Y-m-d',
        value: 'defaultTime',
        hours12: false,
        step : 15,
        yearStart: 1900,
        yearEnd: 2020

    });


    //parse les infos en objet
    user = JSON.parse(user);

    //tableau d'objet liés au form
    const formUser = {

        id : document.getElementById('id'),
        firstname : document.getElementById('firstname'),
        lastname : document.getElementById('lastname'),
        birthday : document.getElementById('birthday'),
        city : document.getElementById('city'),
        picture : document.getElementById('pictures'),
        email : document.getElementById('email'),
        phone : document.getElementById('phone'),
        password : document.getElementById('password'),
        role_id : document.getElementById('role_id'),
        submit : document.getElementById('btn-submit')

    };

    //affichage valeurs liés à l'utilisateur existant via localstorage sur les inputs
    document.getElementById('id').value = `${user.id}`;
    document.getElementById('firstname').value = `${user.firstname}`;
    document.getElementById('lastname').value = `${user.lastname}`;
    document.getElementById('birthday').value = `${user.birthday}`;
    document.getElementById('city').value = `${user.city}`;
    document.getElementById('pictures').value = `${user.picture}`;
    document.getElementById('email').value = `${user.email}`;
    document.getElementById('phone').value = `${user.phone}`;
    document.getElementById('password').value = `${user.password}`;

    //function montrant une erreur 
    function showError(input, message){
        //selectionne le parent du l'input qui est la div pour class name control
        const control = input.parentElement;
        //ajoute le nom de class control error si il y a une erreur
        control.className = "control error";
        //selectionne le classe name perror
        const perror = control.querySelector('.perror');
        // ajoute le message d'erreur à la class perror 
        perror.innerText = message;
    }

    //function montrant un succès
    function showSuccess(input){
        //selectionne le parent de input qui est la div pour class control
        const control = input.parentElement;
        //ajoute à la class control : control success
        control.className = "control success";
    }

    function checkRequired(inputArr){
        inputArr.forEach(function(input){
            if(input.value === ''){
                showError(input, `${getFieldName(input)} est requis`)
            }else{
                showSuccess(input);
            }
        });
    }

    // function checkPhoneNumber(phone){
    //     const phoneCkeck = /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/;
    //     return phoneCheck.test(phone);
    // }

    // //function checkant si l'email rentré dans l'input est valide
    // function isValidEmail(email){
    //     const checkEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    //     return checkEmail.test(String(email).toLowerCase());
    // }

    function getFieldName(input){
        return input.id.charAt(0).toUpperCase() + input.id.slice(1);
    }

    //addeventlistener au clique lors soumission du formulaire pour envoie des valeurs entrée dans input
    formUser.submit.addEventListener('click', (e) =>{

        e.preventDefault();

        checkRequired([formUser.firstname, formUser.lastname, formUser.birthday, formUser.city, formUser.email, formUser.phone, formUser.password]);

        //instanciation ajax xhr
        const xhr = new XMLHttpRequest();

        // chargement des réponses récupéré à la soumission du formulaire
        xhr.onload = () => {

            if(xhr.status == 401){
                
                window.location.replace("<?php echo base_url()?>connexion");
               
            }else{

                if(xhr.status == 200){

                    window.location.replace("<?php echo base_url()?>accueil");

                    //variable vide
                    let responseObject = null;

                    //si succès récupération réponse
                    try {

                        responseObject = JSON.parse(xhr.responseText);

                        console.log(responseObject);

                    //sinon méssage d'erreur
                    } catch (e) {

                        console.error('could not parse JSON!')

                    }

                }
            
            }

        }

        //permet de récupérer les valeurs liés au formulaire pour envoie sur api
        const requestData = `id=${formUser.id.value}&firstname=${formUser.firstname.value}&lastname=${formUser.lastname.value}&birthday=${formUser.birthday.value}&city=${formUser.city.value}&pictures=${formUser.picture.value}&email=${formUser.email.value}&phone=${formUser.phone.value}&password=${formUser.password.value}&role_id=${formUser.role_id.value}`;

        console.log(requestData);

         //envoie sur l'uri ci-dessous
        xhr.open('PUT', domain+'user', true);

        xhr.setRequestHeader("authorization", user.jwt);

        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        //envoie des valeurs liés au formulaire lors de la requête 
        xhr.send(requestData);

    });

}
</script>
</body>
</html>