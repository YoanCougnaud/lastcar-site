<?php

class MY_Model extends CI_Model{
    
    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    public function setProperties($array){
        
        foreach ($array as $key => $value) {

            $method = 'set'.ucfirst($key);

            if(!method_exists($this, $method)){
                
                return false;
                
            }else{
                
                $this->$method($value);

            }
            
        }
    }

}