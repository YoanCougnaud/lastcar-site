<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller{

    public function __construct()
    {
        parent::__construct();
    }

    public function getDomain(){

        $json = json_decode(file_get_contents("../config.json"));

        return $json;

    }

}