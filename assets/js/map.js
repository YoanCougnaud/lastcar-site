
// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

function initMap() {
  //récupération des inputs dans le fichier search.php via leurs id
  let input = document.getElementById('cityfrom');
  let input2 = document.getElementById('cityto');
  //ajout des inputs dans l'objet google map places autocomplete
  new google.maps.places.Autocomplete(input);
  new google.maps.places.Autocomplete(input2);

}

